# Funkce

## Syntaxe

```javascript
function na3(x) {
  return x * x * x;
}
```

## Předávání argumentů (a)

- primitivní typy se předávají hodnotou

```javascript
var x = 10;
function pridej5(x) {
  x = x + 5;
  console.log(x); // 15
}

console.log(x); // 10
pridej5(x);
console.log(x); // 10
```

## Předávání argumentů (b)

- objekty (pole je také objekt) se předávají referencí

```javascript
var animal = { kind: 'dog', limbs: 4 }
function makeOctopus(animal) {
    animal.limbs = 8;
}
makeOctopus(animal);
console.log(animal); // { kind: 'dog', limbs: 8 }
```

```javascript
var cisla = [1,1,2,3,5,8,13];
function f(pole) {
    for(i=0;i<pole.length;i++)
        pole[i]*=2;
    return pole;
}
console.log(f(cisla)); // [2, 2, 4, 6, 10, 16, 26]
console.log(cisla);    // [2, 2, 4, 6, 10, 16, 26]
```

## Function expression

anonymní funkce

```javascript
var x2 = function(x) { return x * x; }
```
nebo může mít jméno

```javascript
var fibonacci = function fib(n) { return n <= 2 ? 1 : fib(n-2) + fib(n-1) };
console.log(fibonacci(8)) // 21
```

## Function expression

funkce jako argument

```javascript
function filter(f, a) {
    var result = [];
    for(var i = 0; i < a.length; i++)
        if(f(a[i])) result.push(a[i])
    return result;
}

var even = function(x) { return x%2==0; };
var filtered = filter(even, [1, 2, 3, 4, 5] ); // [2, 4]
```

## Function hoisting

- pouze pro `function declaration`, nikoliv pro `function expression`

```javascript
f();
function f() { console.log('f'); }
g(); // ReferenceError: g is not defined
g = function() { console.log('g'); }
```

## Platnost proměnných ve funkci

```javascript
var a = 1, b = 2, c = 4;

function x() { return a + b; } // 3
function y() {
    var a = 3, b = 5; // překryje globální `a` a `b`
    return a + b + c; // 12
}
```

## Rekurzivní funkce

- funkce odkazuje sama na sebe
- viz Fibonacci `fib(n)` výše

## Vnořené funkce (closure)

- vnitřní funkce
  - je přístupná jen z vnější funkce (`private`)
  - sdílí kontext (scope) vnější funkce, ale ne naopak
  - tvoří *closure* - funkce včetně prostředí

## Vnořené funkce (closure)

```javascript
function animal(w) {
    var weight = w;
    function update(d) {
        weight += d;
    }
    return {
        eat: function() {
            update(4);
			return weight;
        },
        run: function() {
            update(-5);
 			return weight;
        }
    }
}
var tiger = animal(300);
var zebra = animal(100);
zebra.run()   // 95
tiger.eat()   // 304
zebra.run()   // 90
zebra.weight  // undefined
```

## Argumenty funkce

```javascript
function a() {
    var s = 0;
    for(var i = 0; i < arguments.length; i++) {
        s += arguments[i];
    }
    return s;
}
a(1, 2, 3, 4, 5) // 15
```

## Argumenty funkce

- od ECMAScript 6
- `...x` - zbytek argumentů jako pole

```javascript
function a(n, ...x) {
    return x.map(function(x) { return n * x; });
}
a(3,1,2,3) // [3, 6, 9]
```

## Funkce =>

- od ECMAScript 6

```javascript
function a(n, ...x) {
    //return x.map(function(x) { return n * x; });
    return x.map(x => n * x);
}
a(3,1,2,3) // [3, 6, 9]
```

## Funkce => a this

- funkce je objekt a vnořená funkce má vlastní `this`

```javascript
function Pocitadlo() {
    this.p = 0;
    setInterval(function f() {
        this.p++; //nefunguje, funkce `f` má vlastní this
    }, 1000);
}
```

## Funkce => a this

řešení v ECMAScript 3/5

```javascript
function Pocitadlo() {
    var self = this;
    self.p = 0;
    setInterval(function f() {
        self.p++;
    }, 1000);
}
```

## Funkce => a this

pomocí `=>`

```javascript
function Pocitadlo() {
    this.p = 0;
    setInterval(() => {
        this.p++;
    }, 1000);
}
```

## Zdroje

- [MDN Functions](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Guide/Functions)
