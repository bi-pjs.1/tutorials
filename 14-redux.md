# Redux

## Motivace

Stav aplikace =
- podoba stránky společně se všemi změnami
- DOM elementy a jejich obsah, pořadí, atributy
- javascriptové proměnné
- ...

pro "uložení" stavu používáme

- CSS třídy (`.active`, `.done`)
- `data-*` atributy
- JS globální proměnné, struktury
- ...

=> data nepředvídatelně poschovávána pro různých místech => chaos!

My to chceme [lépe](#pravidla).

(volně podle [React + Redux](https://www.zdrojak.cz/clanky/redux-react-23-redux/))


## Stavební kameny

- **stav aplikace** popsán jednoduchým objektem

    ```
    {
        todos: [
            { text: 'Study', completed: false },
            { text: 'Graduate', completed: false }
        ],
        visibilityFilter: 'SHOW_INCOMPLETE'
    }
    ```

- změny se provádějí pomocí **akcí** = objekt s povinným atributem `type` a volitelně dalšími atributy

    ```
    { type: 'ADD_TODO', text: 'Go to party' }
    { type: 'TOGGLE_TODO', index: 1 }
    { type: 'SET_VISIBILITY_FILTER', filter: 'SHOW_ALL' }
    ```

- akce se provádějí pomocí **reducerů**

    ```
    function visibilityFilter(state = 'SHOW_ALL', action) {
      if (action.type === 'SET_VISIBILITY_FILTER') {
        return action.filter
      } else {
        return state
      }
    }
    ```
    
    sloučení reducerů
    
    ```
    import { combineReducers, createStore } from 'redux';
    import { todos, visibilityFilter } from './reducers';

    const reducers = combineReducers({ todos, visibilityFilter })
    const store = createStore(reducers)
    ```

- pokud reducer umí odbavit akci vrátí nový stav, jinak vrátí původní

## <a name="#pravidla"></a>Tři základní pravidla

1. stav aplikace je uložen na jediném místě - `store`
   - komponenty nezjišťují svůj stav navzájem, dívají se do *store*

      ```
      const store = createStore(reducer)
      // ...
      store.getState()
      ```
   - stav lze jednoduše uložit a načíst příp. implementovat *undo*
1. jediný způsob změny stavu je "emitovat" akci (objekt)
   - nikdy nezapisovat do stavového objektu přímo
   - objekty akcí lze též serializovat příp. "přehrát" znovu
   - když dojde ke změně, komponenty se přerenderují

      ```
      store.dispatch({ type: 'COMPLETE_TODO', index: 1 })
      ```
1. změny probíhají pomocí *prostých* funkcí
   - *prostá* funkce musí pro vstup vracet vždy stejný výstup a nesmí mít vedlejší účinky
   - funkce přijme předchozí stav a akci a vrací nový stav
   - nový stav se vrací pomocí nového objektu, neupravujeme ten původní

## React + Redux

- balíčky `npm install --save redux react-redux`


    ```
    import React from 'react';
    import ReactDOM from 'react-dom';
    import {Provider} from 'react-redux';
    import {createStore} from 'redux';
    
    // ...
    ```
- `Provider` - zabalení původní komponenty do komponenty s uložištěm

    ```
    const App = () => (
        <Provider store={store}>
            <Tasks />
        </Provider>
    );
    ```
- `connect` - propojení Redux stavu s React `props` a propojení Redux `dispatch` s handlery v React `props`

    ```
    // state == store.getState()
    const mapStateToProps = (state) => ({
        user: state.currentUser
    });
        
    // { toggleDone: toggleDone }
    const mapDispatchToProps = {
        toggleDone
    };
        
    export default connect(mapStateToProps, mapDispatchToProps)(Tasks);
    ```

## Odkazy

- [Redux](https://redux.js.org/introduction)
- [Usage with React - Redux](https://redux.js.org/basics/usage-with-react)
- [Redux and React: An Introduction](http://jakesidsmith.com/blog/post/2017-11-18-redux-and-react-an-introduction/)
- [React + Redux](https://www.zdrojak.cz/clanky/redux-react-23-redux/)
- [Pure function](https://medium.com/@jamesjefferyuk/javascript-what-are-pure-functions-4d4d5392d49c)