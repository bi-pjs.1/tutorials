# Použití OOP

## Closure

- funkce, které používají lokální proměnné z „obalujícího“ kontextu (*lexical environment*)

```javascript
function counter() {
    var i = 0;
    function add() {
        i++;
        return i;
    }
    return add;
}
```

- proměnná `i` je dostupná pouze uvnitř `counter`

## Closure

```javascript
var c = counter();
c(); // 1
c(); // 2
var d = counter();
d(); // 1
```

- *closure* emuluje *private* prostředí

## Closure v cyklu

```javascript
var f = [];

function a() {
    for(var i = 0; i < 3; i++) {
        f[i] = function() { console.log(i) };
    }
}

a();
f[0]();    // 3 - pozor!
```

- anonymní funkce přiřazená do `f[i]` je *closure*
- všechny tři funkce sdíli jeden kontext
- hodnota `i` se určuje v okamžiku spuštění `f[i]`

## Closure v cyklu - řešení

- použítí další *closure*
- funkce `c` vytváří vlastní kontext

```javascript
var f = [];

function c(x) {           // function factory
    return function() {
        console.log(x);
    }
}

function a() {
    for(var i = 0; i < 3; i++) {
        f[i] = c(i);
    }
}

a();
f[0]();    // 0
```

## Closure v cyklu - řešení

- od ECMAScript 6

```javascript
var f = [];

function a() {
    for(let i = 0; i < 3; i++) {
        f[i] = function() { console.log(i); }
    }
}

a();
f[0]();    // 0
```

## Metoda v konstruktoru (closure)

- funkce ve funkci je *closure*, někdy ji ale nepotřebujeme

```javascript
function Person(name) {
    this.name = name;
    this.getName = function() {
        return this.name;
    };
}
```

- vytváří se nová při každém zavolání konstruktoru

## Metoda v prototypu (closure)

- pokud je metoda v prototypu, tak se sdílí

```javascript
function Person(name) {
    this.name = name;
}
Person.prototype.getName = function() {
    return this.name;
};
```

## Hierarchie objektů

![](img/07-hierarchy.dot.png)

```javascript
function Employee() {
  this.name = '';
  this.dept = 'general';
}

function Manager() {
  Employee.call(this);
  this.reports = [];
}
Manager.prototype = Object.create(Employee.prototype);
// nebo
Manager.prototype = new Employee;
```

- Varianty nastavení prototypu nejsou ekvivalentní!
- Druhá varianta provádí kód konstruktoru - viz níže

## Hierarchie objektů

```javascript
function Worker() {
  Employee.call(this);
  this.projects = [];
}
Worker.prototype = Object.create(Employee.prototype);

function SalesPerson() {
   Worker.call(this);
   this.dept = "sales";
   this.quota = 100;
}
SalesPerson.prototype = Object.create(Worker.prototype);

function Engineer() {
   Worker.call(this);
   this.dept = "engineering";
   this.machine = "";
}
Engineer.prototype = Object.create(Worker.prototype);
```
- `call` zavolá konstruktor v zadaném kontextu `this`
- srovnej s variantou bez volání `call`

## Přidání atributu prototypu

```javascript
Employee.prototype.speciality = 'none';
Worker.prototype.speciality = 'work';
jim = new Employee();
jack = new Manager();
jane = new Engineer();
console.log(jim.speciality); // none
console.log(jack.speciality); // none
console.log(jane.speciality); // work
```

![](img/07-hierarchy2.dot.png)

## Konstruktor s volitelnými parametry

```javascript
function Employee (name, dept) {
  this.name = name || "";
  this.dept = dept || "general";
}
```

pro inicializaci zděděných atributů je třeba

```javascript
function Worker (name, projs) {
  this.base = Employee;
  this.base(name, "engineering");
  this.projects = projs || [];
}
```

## Konstruktor s volitelnými parametry (2)

konstruktor `Employee` je metodou `Worker` (pod názvem `base`), takže `this` funguje v kontextu `Worker`

```javascript
jack = new Worker('Jack', ['3d printing']);
```

```javascript
Worker {
  base: [Function: Employee],
  name: 'Jack',
  dept: 'engineering',
  projects: [ '3dp' ]
}
```

## Konstruktor s volitelnými parametry (c)

- čistší implementace s `call` nebo `apply`, není potřeba metoda `base`

```javascript
function Worker (name, projs) {
  Employee.call(this, name, "engineering");
  // nebo
  Employee.apply(this, [ name, "engineering" ] );
  // dále stejné
  this.projects = projs || [];
}
```

- vícenásobná dědičnost není možná (ale můžete volat víc „rodičovských“ konstruktorů)

## Hierarchie prototypů znovu

- nemáme (nemusíme mít) vyrobenou hierarchii prototypů:

```javascript
Employee.prototype.bonus = 100;
jack = new Worker('Jack', ['3d printing']);
console.log(Worker.prototype);             // Worker {}
console.log(Worker.prototype.__proto__);   // {}
```

- hierarchii vytvoříme až teď

```javascript
Worker.prototype = Object.create(Employee.prototype); // <==
console.log(Worker.prototype);             // Employee {}
console.log(Worker.prototype.__proto__);   // Employee { bonus: 100 }
john = new Worker('John', ['3d engraving']);
console.log(jack.bonus);                   // undefined
console.log(john.bonus);                   // 100
```

- volání „rodičovského“ konstruktoru v kontextu potomka nastaví atributy přímo potomkovi
- s hierarchii prototypů „zůstává“ atribut u předka

## Hierarchie prototypů znovu (b)

```javascript
Worker.prototype.access = true;
console.log(jack.access);                   // undefined
console.log(john.access);                   // true
console.log(jack.__proto__);                // Worker {}
console.log(john.__proto__);                // Employee { access: true }
console.log(Worker.prototype);              // Employee { access: true }
console.log(jack.__proto__.__proto__);      // {}
console.log(john.__proto__.__proto__);      // Employee { bonus: 100 }
console.log(jack.__proto__.constructor);    // [Function: Worker]
console.log(Worker.prototype.constructor);  // [Function: Employee]
```

## Hierarchie prototypů znovu (c)

![](img/07-hierarchy3.dot.png)

## Hierarchie prototypů znovu (d)

- pokud hierarchii vytvoříme jinak

```javascript
Worker.prototype = new Employee;            // zavolá se konstruktor
john = new Worker('John', ['3d engraving']);
Worker.prototype.access = true;
console.log(john.__proto__);                // Employee { name: '', dept: 'general', access: true }
```

## Porovnávání objektů
- `false` i když má objekt stejné hodnoty atributů

```javascript
var o = { a: 22 };
var p = { a: 22 };
console.log(o == p);    // false
console.log(o === p);   // false
```

- `true` pokud jde o stejnou referenci

```javascript
var q = o;
console.log(o == q);   // true
console.log(o === q);  // true
```

## Serializace atributů

- řetězce, čísla, pravdivostní, objekty, pole

```javascript
address = { stret: 'Dlouhá 1', city: 'Praha'}
john = {
    name: 'John',
    age: 25,
    address: address
}
str = JSON.stringify(john)
// '{"name":"John","age":25,"address":{"stret":"Dlouhá 1","city":"Praha"}}'
```

## Serializace atributů

```javascript
function Person(name) {
    this.name = name;
}
john = new Person('John');    // Person {name: 'John'}
Person.prototype.getName = function() { return this.name; }
john.getName();               // 'John'
str = JSON.stringify(john);   // '{"name":"John"}'
john2 = JSON.parse(str);      // {name: 'John'}
```

- chybí vazba (`__proto__`) na `Person`

## Serializace metod (funkcí)

```javascript
john = {
    name: 'John',
    getName: function() { return this.name; }
}
// '{ name: 'John', getName: [Function: getName] }'
john.getName()
// 'John'
str = JSON.stringify(john)
// '{"name":"John"}'
```

- metoda se do JSON neserializovala!

## Serializace metod (funkcí)

```javascript
function Person(name) {
    this.name = name;
}
Person.prototype.getName = function() { return this.name; }
Person.prototype.toJson = function() { return JSON.stringify(this); }
Person.fromJson = function(str) {
    data = JSON.parse(str);
    return new Person(data.name);
}

john = new Person('John');    // Person {name: 'John'}
str = john.toJson();
john2 = Person.fromJson(str);
john.getName();               // 'John'
john2.getName();              // 'John'
```

- `getName` definujeme na prototypu, stejně jako `toJson`
- `toJson` zde není nutná, ale umožňuje určit co a jak serializovat
- `fromJson` není na prototypu! Vytváří „instanci“ `Person`, takže funguje i `getName`


## Zdroje

- [MDN Closures](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Closures)
- [MDN JSON](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/JSON)
- [Best way to serialize/unserialize objects in JavaScript?](https://stackoverflow.com/questions/6487699/best-way-to-serialize-unserialize-objects-in-javascript)
- [MDN Details of the object model](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Guide/Details_of_the_Object_Model)
- [MDN new operator details](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Guide/Details_of_the_Object_Model#Inheriting_properties)
