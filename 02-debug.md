# Vývojové prostředí, debugování, pracovní workflow

Tento proseminář je zaměřen na praktickou práci s Javascriptem.

## Vývojové prostředí (IDE)

- V učebně nainstalován *WebStorm* (příp. *PhpStorm*), který podporuje zvýrazňování syntaxe a chytré doplňování kódu
- Možnost získat [studentskou licenci](https://www.jetbrains.com/student/)
- Debug režim

## Debugování

- příkaz `debugger` v prohlížeči zastaví běh javascriptu a umožní zjišťění aktuálních hodnot a krokování kódu

### hodnota proměnné

- Výpis hodnot proměnných pomocí `console.log(...)`
  - funguje i pro pole a objekty
  - možnost výpisu více proměnných `console.log(a, b, c, ...)`
- V konzoli prohlížeče nebo interaktivním režimu `node` možno pro zjištění hodnoty také jednoduše zadat název proměnné

```javascript
> john = new Person('John')
Person { name: 'John', projects: [] }
> john.name
'John'
```

### typ proměnné

- operátor `instanceof`

```javascript
> john instanceof Person
true
> john instanceof Object
true
> john instanceof Function
false
```

- operátor `typeof`

```javascript
> typeof Person
'function'
> typeof john
'object'
```