# Document Object Model (DOM)

- rozhranní pro práci s *dokumentem* (webovou stránkou, HTML, XML)
- hierarchická, objektová reprezentace
- umožňuje *dokument* číst a upravovat 
- objekt `window` a `document` (potomek `window`)
- části webobé stránky jsou potomky `document`

## Rozhranní podle typu elementu

- [`Node`](https://developer.mozilla.org/en-US/docs/Web/API/Node) - [`Element`](https://developer.mozilla.org/en-US/docs/Web/API/element) - [`HTMLElement`](https://developer.mozilla.org/en-US/docs/Web/API/HTMLElement)
- poskytuje další specifické metody
  - `HTMLTableElement`, `HTMLParagraphElement`, `HTMLImageElement`, ...

## [atributy `Node`](https://developer.mozilla.org/en-US/docs/Web/API/Node#Properties)

- `childNodes`, `firstChild`, `lastChild`
- `parentNode`, `parentElement`
- `previousSibling`, `nextSibling`
- `nodeName`, `nodeType`
- ...

## [metody `Node`](https://developer.mozilla.org/en-US/docs/Web/API/Node#Methods)

- `appendChild`, `hasChildNodes`, `removeChild`, `replaceChild`
- `insertBefore`


- [procházení všech potomků](https://developer.mozilla.org/en-US/docs/Web/API/Node#Browse_all_child_nodes)

```javascript
function DOMComb (oParent, oCallback) {
  if (oParent.hasChildNodes()) {
    for (var oNode = oParent.firstChild; oNode; oNode = oNode.nextSibling) {
      DOMComb(oNode, oCallback);
    }
  }
  oCallback.call(oParent);
}
```

## [atributy `Element`](https://developer.mozilla.org/en-US/docs/Web/API/element#Properties)

- `className`, `classList` (CSS třídy),
- `id`, `name`
- `tagName`, `localName`
- `innerHTML`, `outerHTML`
- `previousElementSibling`, `nextElementSibling` (`NonDocumentTypeChildNode.*`)
- ...

## [metody `Element`](https://developer.mozilla.org/en-US/docs/Web/API/element#Methods)

- `getElementById` - jeden element
- `getElementsByTagName` - pole
- `getElementsByClassName` - pole
- `hasAttribute`, `hasAttributes`
- `getAttribute`, `setAttribute`, `removeAttribute`
- `addEventListener`, `removeEventListener` (`EventTarget.*`)
- ...

```javascript
var anchors = document.getElementsByTagName('a');
for (a of anchors) {
    console.log(a.getAttribute('href'));
    console.log(a.href);
}
```

## [atributy `HTMLElement`](https://developer.mozilla.org/en-US/docs/Web/API/HTMLElement#Properties)

- `contentEditable`, `isContentEditable`
- `dataset` - vrací kolekci HTML atributů `data-*`
- `style`, `tabIndex`, `title` 

## Zkratka!

```javascript
var el = document.getElementById('CoolContent');
console.log(el);
console.log(CoolContent);
console.log(el === CoolContent);
```

## Javascript v HTML

- inline v atributu

```html
<body onload="console.log('loaded');">
```

- v elementu `script`

```html
<script>
    var tables = document.getElementsByTagName('table');
</script>
```

- samostatný soubor
- i v XHTML může vyžadovat samostatný uzavírací tag `</script>` a nestačí `<script ... />`

```html
<script src="script.js" type="application/javascript"></script>
```

## Základní operace

- příklady (postupně rozšiřované)
  - [Základní manipulace s elementy](example/10-dom.html)
  - [Dynamicky vytvořená tlačítka](example/10-dom2.html)
  - [Sousední uzly](example/10-dom3.html)

## Zdroje

- [Introduction to the DOM](https://developer.mozilla.org/en-US/docs/Web/API/Document_Object_Model/Introduction)
- [Web API](https://developer.mozilla.org/en-US/docs/Web/API)