function handler_bubble(e) {
    console.log('bubble from ' + e.target.id + ' on ' + e.currentTarget.id);
}

function handler_capture(e) {
    console.log('capture from ' + e.target.id + ' on ' + e.currentTarget.id);
}

window.addEventListener('load', function() {
    var divs = document.getElementsByTagName('div');

    for(div of divs) {
        div.addEventListener('click', handler_capture, true);
        div.addEventListener('click', handler_bubble);
    }
});