# Základy OOP - cvičení

> [Teoretická část](../07-objects.md)

## Úkoly

- Vytvořte definice objektů (modelů), které budou reprezentovat datový model aplikace pro správu úkolů (*To-Do*)
- Modely umístěte do samostatného souboru, který budete načítat pomocí `require` (viz příklad [hlavní soubor](../example/02-main2.js), [modely](../example/02-classes2.js))
- Propojte instance dohromady za pomoci vhodných atributů (`User.groups`, `Group.tasks`, ...)
- Vyzkoušejte vytvořit
  - 2 instance `User`
  - 2+ instancí `Group`
  - 4+ instancí `Task`
- Vypište strom objektů

## Modely („třídy“)

(1b)

- `Task`
  - `id` - unikátní identifikátor
  - `title`
  - `description` - delší popis (volitelný)
  - `done` - zda je úkol hotov
  - `group` - skupina (`Group`) do které úkol patří (povinná)
  - `created` - datum vytvoření
  - `due_date` - termín splnění (volitelný)
- `Group`
  - `id` - unikátní identifikátor
  - `title`
  - `owner` - uživatel (`User`), který skupinu vytvořil (povinné)
  - `tasks` - pole úkolů (`Task`) v dané skupině
- `User`
  - `email` - email, slouží jako unikátní identifikátor
  - `groups` - pole skupin (`Group`) daného uživatele

## Doplňující úkoly

(1b)

- Implementujte do prototypů jednotlivých modelů metodu `toString()`
- Vytvořte metodu `checkConsistency`, která projde strom objektů a ověřte konzistenci
  - pro položky `User.groups` platí `Group.owner == User`
  - pro položky `Group.tasks` platí `Task.group == Group`
- Nasimulujte nekonzistenci  
- Vyzkoušejte striktní rovnost `===`