# Závislosti projektu - cvičení

> [Teoretická část](../11-dependency.md)

## `npm` a `webpack`

- Pomocí `npm` nainstalujte [webpack](https://webpack.js.org/) a [webpack-dev-server](https://webpack.js.org/configuration/dev-server/),
případně další závislosti.
- Upravte svůj kód na modulární řešení, využijte přitom [ES6 importy](http://exploringjs.com/es6/ch_modules.html).

  ```
  // src/models/Task.js
  // importujeme to, co potřebujeme odjinud
  import Group from './models/Group';
  // ...
  class Task {
      // ...
  }
  
  export default Task; // exportujeme to, co nabízíme ostatním
  ```
- Pro vytvoření balíčku využijte webpack a následně pomocí vývojového serveru spustťe aplikaci.
  
## Odkazy

- [npm](https://docs.npmjs.com/)
- [webpack](https://webpack.js.org/)
