# Redux - cvičení

> [Teoretická část](../14-redux.md)

- naklonujte si git repozitář s [referenčním řešením](https://gitlab.com/bi-pjs.1/labs) úkolníčku
- cílem je doplnit `taskReducer`, aby podporoval označení úkolu jako hotového
    - může být potřeba v současné implementaci upravit (přidat metodu do třídy `TaskManager`)
- dále je potřeba komponentu `Task` pomocí funkce `connect` spojit s knihovnou Redux
a upravit obsluhu události v komponentě
