# Proměnné a konstanty, výrazy a řídicí struktury - cvičení

> [Teoretická část](../03-operators-expressions.md)

## Celá čísla

1. Napište funkci, která zjistí zda je zadané číslo prvočíslo.
1. **Napište funkci, která vypíše všechna prvočísla menší než zadané číslo.** Využijte funkci z předchozího příkladu.
 
## Desetinná čísla

1. Napište funkci, která porovná dvě zadaná desetinná čísla s přesností zadanou jako třetí argument.
    - Pokud není přesnost zadána použijte předdefinované [`EPSILON`](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Number/EPSILON)
1. Napište funkci, která spočítá zhodnocení vašich úspor po zadaném počtu měsíců, pro zadaný roční úrok.
  
## Řetězce

1. Napište funkci, která určí, zda je zadaný řetězec palindrom (tj. čte se stejně zleva i zprava - *rotor*, *madam*)
    - Uvažujte palindromy liché (*krk*) i sudé (*abba*) délky
    - Upravte předchozí algoritmus tak, aby ignoroval bílé znaky, diakritiku, interpunkci apod.
    - **Použijte algoritmus pro vyhledání palindromů v textu (v tomto případě není ignorování nepohodlných znaků povinné).**
    
## Pole

1. Napište funkci, která pro zadané pole vrátí také pole obsahující četnost jednotlivých prvků vstupu.

```javascript
var a = [1, 2, 3, 2, 4, 4, 0, 1, 'a', 10, 5, 2, 2, 4, 7, 18];
f(a)    // 0: 1, 1: 2, 2: 4, 3: 1, 4: 3, 5: 1, 7: 1, 10: 1, 18: 1, a: 1
```
