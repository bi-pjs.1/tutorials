# React - cvičení

> [Teoretická část](../12-react.md)

- vytvořte minimálně 4 komponenty v knihovně React
  - `Task` pro úkol
  - `Group` pro skupinu úkolů
  - `User` pro uživatele
  - `App` pro vyhledávání uživatelů
- uspořádejte komponenty do hierarchie tak aby to odpovídalo datovému modelu
- komponenty v tuto chvíli nemusí pracovat s reálnými daty, ale můžou staticky vypisovat libovolná data
