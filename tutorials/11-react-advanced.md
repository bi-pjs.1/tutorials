# React Props, State a Lifecycle - cvičení

> [Teoretická část](../13-react-advanced.md)

- vytvořte si data s jedním uživatelem, který bude mít alespoň dvě skupiny a v každé skupině,
alespoň dva úkoly
    - využijte datový model vytvořený v rámci lekce o [objektech](06-objects.md)
- upravte komponenty vytvořené na [přechozím cvičení](10-react.md), aby byly schopné pracovat pomocí props s výše
uvedenými daty
- přidejte zpracování události kliknutí na checkbox (splnění/nesplnění úkoly) v komponentě Task

## Nápověda

```jsx
function NumberList(props) {
  const numbers = props.numbers;
  const listItems = numbers.map((number) =>
    <ListItem key={number.toString()} value={number} />
  );
  return (
    <ul>
      {listItems}
    </ul>
  );
}
```

## Zdroje

- [Lists and Keys - React](https://reactjs.org/docs/lists-and-keys.html)
